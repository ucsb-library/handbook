---
title: DLD Handbook
---

Digital Library Development Handbook
====================================

This handbook is the core source of documentation for the daily engineering practices and procedures within
Digital Library Development. It defines the how and why of our work together. Aspirationally, it serves as
a daily reference for each team member. The handbook is a living document; collaborative updates to its
content are encouraged via [issues][handbook-board] and [Merge Requests][handbook-merge-requests].

Our team strives always to uphold the [UCSB Principles of Community][principles] and the [Digital Library
Development values][dld-values].

<nav id="toc" markdown="1">
## Table of Contents
{:.no_toc}

* …The Table Of Contents…
{:toc}
</nav>

## Collaboration

We value clear, transparent, asynchronous, and frequent communication. Our success depends on our ability to
share our expertise and offer each other assistance in our work.

Our most important channels for communication include:

  - [DLD Slack][dld-slack] is our chat channel and virtual water-cooler.
  - Issues on the [DLD Development Board][dld-board] are the best place for discussion on technical issues or matters
    requiring feedback from the development team. Issues in individual GitLab repositories are automatically added
    to this board..
  - [Merge Requests][dld-mrs] are the best way to propose and discuss changes to code and documentation (including the
    Handbook).
  - Pair Programming
  - DLD Staff Meeting: our weekly face-to-face staff meeting is held on Thursdays at 14:00 Pacific.
    This is usually a 30 minute meeting. Monthly, we hold an hour long extended format meeting. Agenda contributions
    for both formats are open to anyone.


### Collaboration with IT-OPS

Often we collaborate with the Library's Information Technology Operations (IT-Ops) team on projects.  They use Jira for issue tracking while DLD uses Gitlab.  To facilitate cross system issue tracking we have a mechanism for automatic ticket creation and updating in the other team's issue tracker.  To create a shared issue open it in our [Cloud Infrastructure][cloud-infra] project and add the _label_ `sync::jira_ops`.  This will trigger a syncronization with Jira.

### Source Control

We use [`git`][git] for source control across all our projects, and use GitLab as our general development platform.
See [_Collaboration with Git(Lab)_][git+gitlab] for more.

## Deployments and Cloud Infrastructure

We strive to make delivery of software services part of our processes from the eariest
stages of development. [Continuous Delivery][continuous-delivery] makes our development and
operations processes more predictible, but means that DLD engineers take a high degree of
responsibility for our own operational needs. This means we all take responsibility for keeping
our [CI/CD pipelines][ci-cd] functioning commit-to-commit on each project, and that our team
takes ownership of our build processes and deployment environments. We document our
infrastructure in the [Cloud Infrastructure][cloud-infra] project.

## Project Start-up

When there's a need to start a new project, there's a brief [project startup guide and checklist][project-start-up].

## Incident Response

Incidents are inevitable. Our culture is focused on reducing the frequency and severity of incidents through
preparedness and transparency.

Incidents are any event resulting in an unplanned service outage or major service degradation, a security breach,
or data loss. In such cases, we seek to respond quickly to minimize impact.

If you discover an incident, you should immediately acknowledge it with an email to `dld@library.ucsb.edu`.
This notifies the department of the issue and allows us to organize additional action; it also serves to
record the time we discovered the problem. Upon resolution, send a brief email stating the issue has been
resolved as a reply. This serves as notice of resolution.

### Security Incidents

For security incidents, a full incident report must be provided to the Director of DLD via email as soon as possible following
resolution. This report is in addition to the notices of acknowledgment and resolution called for by other incidents.
Delivery of this document enables the Director to respond in accordance with [UC wide][uc-incident-response] and
campus security policies and processes.

An [incident report template][incident-report-template] is provided in this handbook.

## Onboarding

### DevOps interns
Please see the [DevOps intern guide][devops-intern-guide] for more information on how to get started.

### Configuring access to K8s
See the [EKS
guide](https://gitlab.com/ucsb-library/cloud-infra/-/blob/trunk/eks/maintenance.md)
in the cloud-infra repository.

### Other Resources

[UCSB Web Standards Group][ucsb-webguide] for web standards compliance information.

[ci-cd]: ./git+gitlab.md#continuous-integration-and-deployment
[cloud-infra]: https://gitlab.com/groups/ucsb-library/-/merge_requests
[code-review-as-a-relationship-builder]: https://bjk5.com/post/3994859683/code-reviews-as-relationship-builders-a-few-tips
[continuous-delivery]: https://continuousdelivery.com/
[devops-intern-guide]: ./DevOps-intern-guide/help-start-here.md
[dld-board]: https://gitlab.com/groups/ucsb-library/-/boards
[dld-mrs]: https://gitlab.com/groups/ucsb-library/-/merge_requests
[dld-slack]: https://app.slack.com/client/T0H79P81M/DEZ3HMHM0
[dld-staff-meeting]: https://docs.google.com/document/d/1iKG1hsA5ILX2qyeBbRyeJn8AAy3k7lrcd4jgQRzZOEY
[dld-values]: https://docs.google.com/document/d/1zACsGddWUdYUuFmqFPhXBc6wg2amXsiU9Mw8y-znqF8
[git+gitlab]: ./git+gitlab.md
[git]: https://www.git-scm.com/
[handbook-board]: https://gitlab.com/ucsb-library/handbook/-/boards
[handbook-merge-requests]: https://gitlab.com/ucsb-library/handbook/merge_requests
[incident-report-template]: ./templates/security-incident-report.md
[kubernetes]: ./kubernetes.md
[pantheon]: ./pantheon.md
[principles]: http://diversity.ucsb.edu/about/principles.of.community/
[principles]: http://diversity.ucsb.edu/about/principles.of.community/
[project-start-up]: ./project-start.md
[ruby]: ./ruby.md
[uc-incident-response]: https://security.ucop.edu/files/documents/policies/incident-response-standard.pdf
[ucsb-gitlab]: https://gitlab.com/ucsb-library
[ucsb-webguide]: https://webguide.ucsb.edu
