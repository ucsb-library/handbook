# Tools

Our philosophy about developer tooling is hands-off but cooperative. We each use the tools
that suit us best, but we also share.

We try to make sure our projects have a clear "default" development environment, which is
well tested, used, and documented so there is a clear entrypoint when someone needs to ramp
up on a project quickly.

Some of the tools we use are:

## General Tooling

  * [Git][git-scm]: We use git for version control; see the section of this Handbook on
    [Source Control][handbook-scm].
    * [Pro Git][git-pro] is a comprehensive book on Git. Chapters 1, 2, 3, 7 and 8 are
      especially useful.
    * [An introduction to version control for novices using Git. - Software Carpentry Lessons][git-novice] 
  * [fish][fish-shell]: If you need a recommendation for a friendly interactive shell,
    `fish` is good for that.
  * [tmux][tmux]: tmux is good for managing terminal sessions.

## Containers

  * [Docker][docker]: We use `docker` to build and run containers on our development environments.
    * [`docker-compose`][compose]: Some of our projects include `docker-compose` for
      multi-container development environments.
  * [`kaniko`][kaniko]: When we need to build containers in the CI pipeline, we use `kaniko`

## Kubernetes

  * [kubernetes][k8s]: We deploy containers to Kubernetes, and develop our applications to
    run there.
    * [Kubernetes: Up and Running][k8s-up] is recommended as a "dive into" style Kubernetes book.
  * [`k9s`][k9s]: Some of us use k9s as a terminal dashboard to Kubernetes.
  * [Helm][helm]: We usually deploy to Kubernetes with Helm Charts, which makes it easier for
    us to quickly deploy sandbox and review applications, as well as manage our Staging and
    Production environments.

[compose]: https://docs.docker.com/compose/
[docker]: https://www.docker.com/get-started
[fish-shell]: https://fishshell.com/
[git-scm]: https://git-scm.com/
[git-pro]: https://git-scm.com/book
[handbook-scm]: ./README.md#source-control
[helm]: https://helm.sh
[k8s]: https://kubernetes.io
[k8s-up]: https://learning.oreilly.com/library/view/kubernetes-up-and/9781492046523/titlepage01.html
[k9s]: https://k9scli.io/
[kaniko]: https://github.com/GoogleContainerTools/kaniko
[tmux]: https://github.com/tmux/tmux/wiki
[git-novice]: http://swcarpentry.github.io/git-novice/