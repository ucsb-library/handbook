---
title: Pantheon hosting
---

# Pantheon hosting

[Pantheon][pantheon] provides hosting for Wordpress and Drupal based websites. You can also host a basic HTML site on Pantheon. The UCSB Enterprise Technology Services (ETS) contracts with Pantheon to host sites for the campus. There is no recharge to the departments for this service.

Currently (2021-February) the library uses Pantheon for the the [LAUC][lauc] site which is based on Wordpress and is in the process of moving the main [Library Website][library website], a Drupal site to Pantheon as well.

[Pantheon platform][pantheon platform] uses a containerized infrastructure on Google Cloud Platform.

### Dashboard

The [Pantheon Dashboard][pantheon dashboard] is the starting point for working with sites hosted on Pantheon.

### Upstream Updates

One of the valuable features of Pantheon is they provide updates to Drupal and Wordpress core soon after they become availalbe. These are most often minor upgrades for security patches or bug fixes. This spares the Library DLD team from having to apply these patches.

These updates are applied automatically to the _Master_ branch in the DEV instance of the site. It is then up to the individual site administrator to deploy the update to TEST and Live instances of the site.

#### Incorporating upstream updates into codebase.

Upon completion of issue #[155](https://gitlab.com/ucsb-library/library-website/-/issues/155) the system will automatically pull upstream updates into our Gitlab based code repository.

In lieu of an automatic routine use this procedure:

1. When an upstream update becomes available apply the updates using the pantheon dashboard or terminus: `upstream:updates:apply <site>.<env> --accept-upstream --updatedb`.  Applying the upstream changes makes commit(s) to the master branch on Pantheon
1. `git pull` those changes from Pantheon to a local instance of the git repo.
2. Then merge the `master` branch into the `trunk` branch 
1. `git push` the trunk branch to Gitlab. This should trigger an automatic deploy to the "trunk" multidev instance back on Pantheon.
1. push the `master` branch to Gitlab as well.


Pantheon [Upstream Updates][upstream updates] Documentation

### Multidev

[Pantheon Multidev][pantheon multidev] feature allows for fully functional instances of a site from a git branch. We can use this feature to provide review deployment of the site.

### Terminus

[Terminus][pantheon terminus] is Pantheon's command line utility. Developers should familiarize themselves with this tool if frequently working on Pantheon. Most operations one can perform via the [Pantheon Dashboard][pantheon dashboard] can be done using Terminus.

[pantheon]: https://pantheon.io
[pantheon multidev]: https://pantheon.io/docs/multidev
[pantheon terminus]: https://pantheon.io/docs/terminus
[pantheon platform]: https://pantheon.io/docs/platform
[pantheon dashboard]: https://dashboard.pantheon.io/
[upstream updates]: https://pantheon.io/docs/core-updates
[lauc]:https://lauc.library.ucsb.edu
[library website]:https://www.library.ucsb.edu/