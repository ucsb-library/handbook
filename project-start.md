---
title: Project Start-up
---

# Project Start-up

 - [ ] Create a gitlab project in the `ucsb-library` namespace;
 - [ ] Initialize a repository, and [change the default branch to `trunk`][trunk];
 - [ ] Ensure appropriate DLD developers and other collaborators have access to the project;
 - [ ] Add issue from this project's [`templates/issue_templates` directory][issue-templates] to `.gitlab/issue_templates`;

 [issue-templates]: ./templates/issue_templates
 [trunk]: (new) https://datatracker.ietf.org/doc/html/draft-knodel-terminology-01 
 
          (old) https://tools.ietf.org/id/draft-knodel-terminology-01.html#master-slave 
