Building this Handbook as a Static Site
=======================================

This handbook is formatted to work as a [Jekyll][jekyll] site. Build and run it
with `docker-compose`:

```sh
docker-compose up
```

This will setup the website with synchronization to your local development copy on http://localhost:4000

[jekyll]: https://jekyllrb.com
