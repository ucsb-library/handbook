---
title: Kubernetes
---

# Kubernetes

DLD applications are deployed into a [Kubernetes](https://kubernetes.io) cluster
managed by [AWS](https://docs.aws.amazon.com/eks/latest/userguide/what-is-eks.html).

## Deploying applications into the cluster

Deploys are run by our [CI/CD
pipeline](./git+gitlab.html#continuous-integration-and-deployment), allowing us to
perform QA on review deployments and automatically have merged code deployed to
staging and production instances.

### Naming conventions

Deployments are primarily categorized by their namespace.  For example, the
Cylinders project has three namespaces, corresponding to each step of the CI/CD
pipeline:
- `cylinders-review`
- `cylinders-staging`
- `cylinders-prod`

This makes monitoring deployments easier and makes mistakes less likely during
manual intervention.

Individual deployments are named too, in order that they can be programmatically
updated and uninstalled by Helm.  Review deployments are named using the
[project name plus the MR
number](https://gitlab.com/ucsb-library/cylinders/-/blob/trunk/ci/cylinders/review.yml#L8),
while staging and production deploys are given a [static
name](https://gitlab.com/ucsb-library/cylinders/-/blob/trunk/ci/cylinders/production.yml#L20)
(since there will only be one of each).
