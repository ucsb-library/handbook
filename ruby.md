# Writing Ruby Code

## Style

We enforce the default code style prescribed by
[Standard](https://github.com/testdouble/standard).  Add it to the Gemfile of
new projects:

```ruby
group :development, :test do
  gem "standard", "1.2.0"
end
```

Then it can be run locally to identify and fix style violations:
```
bundle exec standardrb --fix
```

The CI/CD pipeline for a Ruby project should also include a [linting
stage](https://gitlab.com/surfliner/surfliner/-/blob/trunk/ci/comet/lint.yml#L23),
where `bundle exec standardrb` must pass before an MR can be merged.
