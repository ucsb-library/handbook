## Summary:

*Include a high level narrative summary including the nature of the breach, when and how the incident occured, staff and stakeholders involved, and information about resolution or current status.*

## Timeline:

*Provide a detailed timeline. This should include when the issue began, time to acknowledgement, and time to resolution. To the degree possible, precise times should be given for these, and any major events and responsive actions taken.*

## Impact:

*Give detailed information about the nature of the breach and impact. This should not be speculative or editorialized: stick to the facts.*

## Next Steps:

*List any further responsive actions to be taken following this report.*
