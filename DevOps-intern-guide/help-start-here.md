Welcome DevOps Intern!
=======================

Please see below for a compilation of useful documents and resources to help you learn the duties and responsibilities of being the UCSB Library DevOps Intern.

## Table of Contents
* [Getting Started](#getting-started)
    * [Essentials](#essentials)
      * [Meetings](#meetings)
      * [What is DevOps?](#devops)
      * [Set Up](#set-up)
      * [Resources and Training](#essential-resources-+-training)

## Getting Started
To get started, please read the information below for specifics on collaborative meetings, the essential training needed along with additional information on the program, and learning how to set up your development environment to start working on projects.

Technologies we use:
- Kubernetes
- Docker
- Git + GitLab


### Essentials
- Make sure you have [Slack][dld-slack] downloaded as this is the main line of communication with the DLD team
- This position is a hybrid-remote which means most times everyone will be working in different locations.
- When needed, please refer to the [DLD DevOps Internship Job Description][job-desc] for job functions and duties. Also look at the CAS Learning Outcomes to identify any goals you would like to work on during your duration within the program.
- Please see the attached [Interns Corner][interns-corner] document for advice, tips, and additional information from other interns.  *Also feel free to add into it!*

### Meetings
Since DLD is a hybrid-remote team, meetings are held throughout the week. Meetings include:

DSUs: Daily Standups
* Brief meetings typically 10 to 30 minutes max. A separate DLD DSU will take place after a cross campus project/outside projects DSU.
* Format is as follows:
    * What did you work on yesterday?
    * What will you work on today?
    * Any questions, concerns, or areas you are feeling stuck on?

DLD Department Meetings
* A department meeting that will consist of the DLD team which lasts about 30 minutes, but is given a time allotment of 50 minutes in case there is a need for a longer duration.
* A google doc will be provided with meeting notes and typically a member takes up one of three roles: Facilitator, Notekeeper, and Timekeeper.
    * Facilitator: Hosts and holds the meeting
    * Notekeeper: Takes notes of what is being discussed
    * Timekeeper: Ensures that the meeting is within designated time frame
* Typically within the DLD Department meeting, an agenda is proposed and members are able to add items within the agenda that they would like to discuss during the meeting.
    * Topics discussed include administrative updates and project updates.
    * There is also an Action Item review which is looking at previous departmental meeting actionable items to check if they are completed.
    * If there are items left that weren't able to be discussed in the meeting, those items can be carried over to the next meeting or discussed in a separate meeting.
* All hands- a meeting where all members get together for strategic planning and collaborative activities in person (as part of the members are remote and a part of the team are local)
* Round Robin: Similar to DSU's where you will give updates on what you have worked on and completed (not only daily, but from the last meeting). Letting the two teams (two parts of DLD) update each other on what is being worked on.
* At the end of the meeting, we try to share happy things or funny things that have happened from the week.

DLD Sprint Planning
* A sprint is another word for a set period of time for a project (a work cycle without hard deadlines, just a way to organize work and focus on it within a period of time)
* Sprint meetings aim to set goals for what we're going to work on during the next two week sprint.
* The meetings serve to help prioritize sprints.
    * DLD Sprint Planning Meetings typically happen in 2 week increments.

Sprint Review
* Will summarize the work that has been accomplished. At the end, there is a recording of the work (for us).
* Go over what happened during the sprint.
* Written summary + video demo. To show visible changes for others
* Essentially, it is a review for everyone to show stakeholders.

    *  Stakeholders: anyone who have a stake (interest) in the product
        - something they are using, it's a tool they will be using, they have content they want to share.
        - They aren't working on the project itself.


DLD Retrospectives
* A retrospective is a standard meeting in the Agile methodology framework.
    * DLD uses Retrium to go through phases in the meeting
    * Retrium is like an app where users can post sticky notes and brainstorm ideas. Topics usually include what the team should keep doing, stop doing, or start doing.
        * First phase: everyone has a few minutes to address the topics on the screen.
            * Sticky notes will appear in different colors. You can see typing and deleting, but won't be able to see the actual text until this phase is complete.
        * Second Phase: at the end of the few minutes, sticky notes will become visible for grouping. Grouping the sticky notes will help categorize similar ideas together.
            * Grouping can be done using your cursor to drag and drop sticky notes on top of each other.
            * After this is done, there is anonymous voting.
                * You can vote on the same thing more than once.
                * To vote, click the (+) sign next to the sticky note you want to vote for. A circle should appear.
                * To remove your vote, click on the circle that appeared. It should have an (x) when you hover over it with your mouse.
        * Third Phase: the amount of votes are shown and the topics that are most voted for show on the screen one by one to be discussed.
            * A list of actionable items can be created per topic.

Backlog Refinement Meeting
 - This meeting is for the people responsible for the products to have enough preparation. This allows for when we go into sprint planning with the team, we are well prepared to talk about the upcoming work.
 - We mainly try to make sure the work on the board is organized with work we would like to do.
 - We want to then prioritize certain things on it.
 - Helps us go back and see if things are actually 'Ready'/ see if we missed anything on the Work Board.
 - In short, this meeting is meant to organize everything to make sprint planning a bit easier and go smoother.

### DevOps
What is DevOps?
The DevOps handbook gives a great description as to what DevOps is. The DevOps Handbook Link is accessible through UCSB Library at https://www.oreilly.com/library/view/temporary-access/.

"DevOps shows us that when we have the right architecture, the right technical practices, and the right cultural norms, small teams of developers are able to quickly, safely, and independently develop, integrate, test, and deploy changes into production."

* Instead of working on large batches of a project and testing all at once towards the end, working in smaller increments helps build quality and reduces the amount of errors to be fixed at the end of the project.

"In the DevOps ideal, developers receive fast, constant feedback on their work, which enables them to quickly and independently implement, integrate, and validate their code, and have the code deployed into the production environment (either by deploying the code themselves or by others)."
* In other words, because things are always checked and fixed during the working process, we are able to fix them on the spot and see how the code works with everything else at the moment, not at the end (where problems can occur such as an unsuccessful integration)

"To help us see where work is flowing well and where work is queued or stalled, we need to make our work as visible as possible. One of the best methods of doing this is using visual work boards, such as kanban boards or sprint planning boards, where we can represent work on physical or electronic cards."
* This is where tickets come in. Refer to this [image][tickets] with tickets.

    * Tickets: things to work on in a sprint. Think of it like things in a to do list with different priorities. These tickets will move from left to right. (New --> Complete)

By putting all work for each work center in queues and making it visible, we can easily prioritize work by seeing the bigger goal. In other words, we can work on the highest priority items until it is completed. (Working towards the goal step-by-step)

Another thing to note: Limit "Work In Progress" (WIP)
* "Limiting WIP also makes it easier to see problems that prevent the completion of work. For instance, when we limit WIP, we find that we may have nothing to do because we are waiting on someone else. Although it may be tempting to start new work (i.e., “It’s better to be doing something than nothing”), a far better action would be to find out what is causing the delay and help fix that problem. Bad multitasking often occurs when people are assigned to multiple projects, resulting in many prioritization problems."

### Docker
##### What is Docker?
Docker is a platform for running and developing applications that is pretty much essential for all DLD applications. The main usage of Docker is to allow applications to run in containers, environments isolated from one's operating system. Containers contain every dependency its running application requires, so it doesn't need to rely on what's installed on the user's system. 

Docker is especially useful for CI/CD (continuous integration/continuous delivery) workflows like what we use for our DLD projects due to the ease of using Docker containers in development, testing, and deployment environments. Containers can be easily pushed between development and testing environments while troubleshooting bugs, and then deployment of the application can be as simple as pushing an updated image to a production environment. 

##### What is an image?
A Docker image is a set of instructions used to create a Docker container. Images can be based on existing images, such as a postgres image created from a bitnami registry, or images can be made from scratch, with instructions usually written in a Dockerfile in the project's home directory. 

##### More on Containers
A container is a runnable instance of an image. Although containers are isolated from other containers and the system it is running on by default, the container can be configured to talk to and work together with other containers, such as databases and storage. Often times, when working together with different containers that need to communicate with each other, Docker Compose is more efficient to use. 

##### Docker Compose
Docker compose allows you to run multiple containers simultaneously with more efficient communication between containers. The configuration for how different containers talk to each other is defined in a docker-compose.yaml file. For example, a docker-compose.yaml file can contain configurations for a web server, a db_migrate database migration tool, and a postgres server. These separate images will communicate and depend on each other according to the instructions isolated in yaml file, and thus is more efficient than creating these images separately and having the images interact with each other through external controls. 

To get started with Docker, you can install it [here][Docker].

### Kubernetes
##### What is Kubernetes?
Kubernetes is an orchestration system for running and managing containerized applications in a very controlled environment, essentially a more advanced version of Docker compose. It has many more configurable options for bringing up many containers simultaneously, such as which containers specifically to include, which version of the application to run, specify what to do if one container crashes, and much more. 

##### Kubernetes Terminology
When discussing Kubernetes, several terms are used to talk about abstract parts of a Kubernetes system that allows it to function, such as clusters, nodes, pods, and deployments.

##### Clusters
A Kubernetes Cluster refers to a cluster of computers (either physical or virtual machines), each containerized separately through Docker images. Each cluster contains a control plane, which coordinates the cluster, and worker machines called nodes that run applications.

##### Nodes
Kubernetes nodes are physical or virtual machines that act as worker machines in a cluster. Each node has tools--usually through Docker--for handling container operations, such as pulling a container image from a registry, unpacking the container, and running the application. Applications deployed through Kubernetes will have the control plane schedule containers to run on the nodes of the cluster. Specifically, containers run in pods, which run in nodes. 

##### Pods
A Kubernetes pod is a collection of application containers grouped together with their shared resources/dependencies, which includes shared storage (volumes), a unique pod IP address, and instructions on how to run each container. Each pod may have multiple volumes and containers, but only one IP identification. Each node may contain many pods, but for DLD most nodes contain only one pod and one container. A helpful diagram showing the layout of nodes and pods in a Kubernetes cluster can be found [here][nodes-pods]. 

##### Deployments
A Kubernetes deployment configuration tells Kubernetes how to manage, create, and update instances of your application containers. The Kubernetes deployment controller will constantly overlook the running nodes of the deployment, and if any container goes down, the deployment controller will automatically replace the failed node, providing a self-regulating process to combat application failures. 

This [website][kubernetes-practice] provides some helpful guidance on basic Kubernetes commands and their functions. 

To get started with Kubernetes, you can download [kubectl][Kubectl], a command-line tool used to run commands against Kubernetes clusters. 

### Set Up
Here you will find information on how to set up your development environment. You may also schedule a 1:1 meeting for this if needed. Please see the following information for Mac and Windows.
 * Mac instructions:
    * Go to Finder and find ‘terminal’
    * Install Homebrew on [brew.sh][brew] by copying the link and pasting it into Terminal
    * Once that is completed, type `brew install git` to install Git
    * Once that is completed, you now have Git!

* Windows Instructions:
    * Install [gitforwindows][git-windows]
    * Once it’s downloaded, it will give a lot of different options to set up, keep all of the settings as it shows until you get to the text editor options.
    * Nano or VSCode as a text editor is highly recommended as they are the easiest to grasp.
    * Once you pick a text editor, continue clicking 'next' until it is installed.
    * Git Bash will show on your desktop or be downloaded into your files. Open Git Bash to start using Git!

### Essential Resources + Training
Now that you have your development environment set up, let's try some things out with Git. You will find information below with various links on how to understand and learn Git.
* Git is a version control system that tracks changes within files/ directories.
    * This [youtube video][yt-1] explains this concept fairly well.
    * He mentions a 'central source' (GitHub) which allows teams to collaborate on a public server. DLD uses GitLab, which is very similar to GitHub.
    * Also see this [video][git-15] to learn Git in 15 minutes

##### Configuration
* Now that you have a general idea of what Git is, just as the 2nd video showed, you will need to configure your terminal.
    * To do this, type: `git config --global user.name "-insertname-"`
        * What this command does is register your name within the "____" (quotations)
    * Next, you will have to register your email.
        * To do this, type: `git config --global user.email "-insertemail-"`
        *  hat this command does is register your email within the "____" (quotations)
* NOTE: It doesn't matter whether you use '____' or  " ____ ", but you have to be consistent, otherwise terminal will not recognize what you are trying to do. (Ex: Do not use " ___ ')

##### Useful terms to know
Repository: This is a central file storage location. It is a container that holds everything related to your project. In other words, it is a place where files and folders are stored.
* local repository: local to you, which means it is on your device (laptop/PC)
* remote repository ('origin'): the files are held on a server (GitHub, GitLab, etc)
    * having a remote repository allows many people to work on a project at the same time.

Branches: these are versions of the repository. There will be one main branch which will be the origin. If you are making changes on this branch, you will only have that version.
* What if you want to try something out? Make a branch.
* By making branches, you are making multiple versions of the repository. They are all seperate from each other.
    * EX: You make two branches, (A) and (B). Let's say you are on branch (A) and make a text file and write some text in it. Then you move to branch (B), you will notice that your text file won't be there because the version with the text file is saved onto branch (A).

-more to be added after-




##### Common Commands
`pwd`: print working directory
* This allows you to see where you currently are in your terminal and where you are working at.
    * Ex: if working on the handbook (on Mac), it will print out: `/Users/name/foldername/handbook`

`ls`: lists files within the directory that you are currently on.
 * Ex: If you are in the DevOps Intern Guide and use *ls* in your terminal, `the devops-job-description and help-start-here.md `files will be listed.

`cd`: change directory
 * Ex: if you use: *cd ..* it will bring you from your current folder, to the parent folder. In other words, say you have a folder (A) and you open it. You find that inside has files and another folder (B). You open that folder (B) and are working on the files within that sub folder (B). If you want to return back to the parent folder (A), you will use: `cd ..`

`git status`: this will let you see which changes have been staged, which haven't, and which files aren't being tracked by Git. More specifically, files go through essentially stages.
* Untracked
* Unmodified
* Modified
* Staged

See this [image] for how a file moves through stages.

Many times, you will be editing an existing file. (unmodified--> modified). When you use: *git status*, it will show up in your terminal in red font. To commit your changes, you will have to add the file. See below for how to do that.

`git add`: this adds your changes into the staging area (modified-->staged) which means it will be ready to commit.
* If you made edits to this file, you would get:
  ###### modified:   DevOps-intern-guide/help-start-here.md
  in red text (when using `git status`) and when you add it, the text will turn green (when you use `git status` again).

`git commit`: this saves your changes locally.
* (This means that only you have it on your system, and to share it publicly, you will have to push it. See next command: `git push`)
* This [website's first lesson][git-int-branch] shows you what happens visually when you commit something.

`git push`: this lets you upload your local content into a remote repository.
* What this means is: you move/publish your work onto a platform where it can be shared with anyone.
    * Ex: Github and Gitlab are public servers used to share work.
* Likewise, if you `git pull` something from GitHub or GitLab, it will make a local copy of that public repository.

See this [website][git-commands] for these commands and others that may prove to be useful.





[dld-slack]: https://slack.com/downloads
[job-desc]: ./devops-job-description.md
[interns-corner]: https://docs.google.com/document/d/1fikkGcb0pcoXhMKP5FDrbbLivCVHpvGhoBMzGyPm8AE/edit
[git-windows]: https://gitforwindows.org/
[brew]:https://brew.sh/
[yt-1]: https://www.youtube.com/watch?v=uUuTYDg9XoI
[git-15]: https://www.youtube.com/watch?v=USjZcfj8yxE
[git-commands]:https://www.git-tower.com/learn/git/ebook/en/command-line/appendix/command-line-101
[image]:https://git-scm.com/book/en/v2/images/lifecycle.png
[git-int-branch]:https://learngitbranching.js.org/
[tickets]:https://azurecomcdn.azureedge.net/cvt-ae8aac98df3811fe54cb32dd855f00a55a1d5b1107e88981f18f5b02957878f7/images/page/services/devops/index/boards.jpg
[kubernetes-practice]:https://kubernetes.io/docs/tutorials/kubernetes-basics/
[nodes-pods]:https://d33wubrfki0l68.cloudfront.net/5cb72d407cbe2755e581b6de757e0d81760d5b86/a9df9/docs/tutorials/kubernetes-basics/public/images/module_03_nodes.svg
[Docker]:https://docs.docker.com/get-docker/
[Kubectl]:https://kubernetes.io/docs/tasks/tools/
