Digital Library Development DevOps Internship - Library 
=============================
### GENERAL SUMMARY 
This summer, in collaboration with various departments across campus, the Office of the Chief Information Officer (OCIO) is kicking off the UCSB IT Internship Program matching six undergraduates to projects preparing them for careers in the expanding field of information technology. In addition to individualized year-long projects, the cohort of six interns will experience the benefits of camaraderie through group training on topics such as: cybersecurity, data analytics, Amazon Web Services, Emotional Intelligence, Family Educational Rights and Privacy Act (FERPA), Grant Accounting and Research Administration, Crucial Conversations and Conflict Management, project management, and business analysis. Providing training similar to that found in Fortune 1000 organizations and technology firms; the UCSB IT Internship Program offers a collaborative environment for professional development enhancing and deepening the intern’s curricular and professional experience. While there is no experience necessary to apply for the UCSB IT Internship Program, the participants must demonstrate an eagerness to learn and a willingness to develop an esprit de corps. 


### PROGRAM DESCRIPTION 

The UC Santa Barbara Library’s Digital Library Development (DLD) department is responsible for developing digital platforms, tools, and content to support teaching, research, and learning. The DLD engineering team is a small, hybrid-remote group of developers that works collaboratively to develop and deploy software systems supporting this mission. We use Agile- and DevOps-aware methodologies and technologies including Ruby/Rails, Docker, and Kubernetes. Our work frequently involves collaboration with other UC campuses and Open Source partners, and much of our code is hosted in public (GitLab) repositories. 


### JOB FUNCTIONS AND DUTIES

 Under the close supervision of the Sr. Digital Library Systems Developer, the DevOps Intern is embedded within the DLD engineering team to participate in the software lifecycle from development to production. With support from mentors and collaborators, the intern will work to improve build/test/deploy pipelines across a variety of web applications and other library systems. The intern will be exposed to DevOps 
concepts and have the opportunity to apply them to cloud-native computing environments. 

### JOB QUALIFICATIONS 

##### Requirements: 
* Strong desire to gain experience with DevOps processes, tools and their role in cloud-native application development. 
* Ability to learn effectively and rapidly apply new knowledge to complex problems. 
* Strong written and verbal communication skills. 
Preferred: 
* General understanding of a version control system (e.g. Git). 
* Basic knowledge of a high-level programming language such as Ruby, Python, PHP or Java. 

##### Eligibility: 
* A registered UCSB freshman, sophomore, or junior undergraduate working towards a Bachelor’s degree in any field 
* Must be available to work near to full-time Monday - Friday between May/June 2020 – August/September 2020 (ability to attend Summer courses will be granted) 

### CAS LEARNING OUTCOMES 
The Council for the Advancement of Standards in Higher Education (CAS) promotes standards to enhance opportunities for student learning and development from higher education programs and services. UC Santa Barbara supports student learning outcomes both in an academic setting as well as through co-curricular involvement. This position will facilitate the following learning outcomes to better prepare participants for the rest of their collegiate experiences and professional lives: 

* Knowledge acquisition, construction, integration, and application: Participants will gain understanding and knowledge from a range of disciplines and human experiences, and will be able to connect this knowledge to other information, ideas, and experiences as it relates to daily life (both personally and professionally). 
* Cognitive complexity: IT Intern will demonstrate critical and reflective thinking through discussions surrounding campus climate and the student experience, project management, and front desk operations; this position will gather complex information from a variety of sources (personal experiences, observations, etc.) to form a decision and opinion. This position will facilitate creative processes to not only gain insight into campus life, but to formulate new approaches to project management and job functions. 
* Intrapersonal development: through various activities, interns will look introspectively for realistic self-appraisal and self-understanding as it relates to personal and professional development. Interns gain insight into identity development through discussions on diversity and building community, assisting others on projects, and serving the campus as the faces of Student Life. 
* Interpersonal development: through employment as an IT Intern, students will establish meaningful relationships with other students, student staff, professional staff, University administrators, and community members. The position necessitates working collaboratively while fostering a sense of interdependence. There will be joint projects where interns will need to work together or with other constituents to identify and achieve collective goals. 
* Humanitarianism and civic engagement: The University serves a diverse population and as part of the IT team, interns represent and serve diverse persons. It is important for interns to not only understand one’s own identity and culture, but to seek involvement with persons different than oneself. Duties often encompass an assortment of topics and perspectives, and interns should be prepared to talk about the civic and social responsibility to recognize social systems and their influences on people, to challenge the unfair and unjust, to articulate the values and principles in personal decision making, and to engage in critical reflections and discussions surrounding multifaceted subject matter. 
* Practical competence: as with any co-curricular involvement and employment, IT Interns will gain and practice practical skill development such as effective communication, time management, professionalism, technical competence, and goal creation/pursuit. 
