---
title: Issue Tracking with IT-OPS
---

the DLD team uses [Gitlab issue tracking][gitlab-issue-tracking] to organize our efforts and manage our projects.

The IT-OPS team uses Jira for their issue tracking.  Often our two teams need to collaborate on issues.  To that end we've setup an automatic integration using [Unito][unito] between Gitlab and Jira.   

In order to invoke the integration from Gitlab the issue must be in the [`cloud-infra`][cloud-infra-project] project and have the label: `sync jira_ops`.  Once these criteria are met an issue created in Gitlab will be duplicated in Jira and given a unique Jira identifier.  Subsequent updates to the issue in Gitlab or in Jira will be reflected in the other system.

[Issue 53][issue-53] is a good example of using this integration.


[gitlab-issue-tracking]: https://docs.gitlab.com/ee/user/project/issues/
[cloud-infra-project]: https://gitlab.com/ucsb-library/cloud-infra/-/issues
[unito]: https://unito.io/connectors/jira/
[issue-53]: https://gitlab.com/ucsb-library/cloud-infra/-/issues/53

