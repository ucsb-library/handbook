FROM ruby:3.3

RUN mkdir /handbook

WORKDIR /handbook

COPY Gemfile .
COPY Gemfile.lock .

RUN bundle install

COPY . /handbook

RUN bundle exec jekyll build -d public

EXPOSE 4000

CMD bundle exec jekyll serve -w -d public --host=0.0.0.0
