Collaboration with Git(Lab)
===========================

We use [`git`][git] for source control across all our projects. `git` provides
powerful tools for managing and analyzing commit history—provided the history
is consistently organized, descriptive, and self-contained. The enormous
time-saving utility of tools like `git blame` and `git bisect` depends on a
team-wide effort to produce a legible, context-rich history.

## Projects and Repositories

Generally, `git` repositories are maintained at the project-level. Application
code, maintenance scripts, CI/CD configuration, infrastructure-as-code, etc…
for a given project should be collocated within the repository. The central
version of each repository is hosted as a *project* at in the
[UCSB Library GitLab][ucsb-gitlab].

Repositories should be maintained for team use and ready deployability. This
means:

- Avoid including files or configuration specific to your development
  environment;
- Delete feature branches after merging;
- Perform work in a feature branch;
- Submit code for review often to incorporate your work into the mainline;
- Failing that, rebase frequently to incorporate upstream changes.

Repositories should also be self-documenting. Project descriptions, developer
startup, and deployment considerations should be documented in places reachable
from the project `README.md`. Where the project’s practices differ from
departmental practice as documented in this Handbook, this documentation should
be **unmissable**.

## Issue Boards
We use Issue Boards to help us plan, organize, and visualize our work.
While each project has its own board, we most frequently collaborate
using our [group-level
board](https://gitlab.com/groups/ucsb-library/-/boards/1188047). During
an active sprint, the board is restricted to issues associated with the
milestone for the sprint.

The majority of our boards are organized using the same set of six
columns which parallel our development workflow: 

1. Open: Issues in the Open column are not yet ready to be worked on.
This may be because they are blocked, awaiting further description, or
are missing acceptance criteria. 
    - Generally issues in this column should be unassigned. 
    - Once an issue is ready to be worked on, it should be moved to
Ready.    

1. Ready: Issues in the Ready column are ready to be worked on.
Self-assignment to issues while on sprint is encouraged. 
    - Generally, issues in this column should be unassigned unless the
work described must be completed by a specific individual. 
    - Once beginning work on an issue, it should be moved to In
Progress. 

1. In Progress: Issues in the In Progress column are actively being
worked on. If you stop working on an issue, it should be unassigned and
moved back to Open for another team member to pick up. (Any progress
made should be clearly documented in the issue.) 
    - Issues in this column should be assigned to the individual(s) who
are actively working on the issue. 
    - Once all work on an issue is complete and a merge request created,
it should be moved to Review.

1. Review: Issues in the Review column are awaiting code or stakeholder
review. If additional work is needed on an issue, it should be moved
back to Open or In Progress as appropriate. 
    - Issues in this column should be assigned to either the individual
performing the code review, or to the individual coordinating with
external stakeholders. If an issue in this column is unassigned, this
indicates that someone needs to pick up the review. 
    - For projects using continuous deployment, the issue can be closed
following review and merger. For projects not using continuous
deployment, the issue should be moved to To Deploy.

1. To Deploy: Issues in this column indicate that a manual deployment is
needed. This column is only used for projects not doing continuous
deployment.  (Note: Project-level boards for projects doing continuous
deployment will not have this column.) 
    - Some combination of the author of the change and the reviewer of
the change are typically responsible for the deploy process and the
issue assigned to them. Unassigned issues in this column are an
indication that someone needs to pick up the deployment. 
    - Once changes are successfully deployed, the issue can be closed.

1. Closed: Issues in this column represent done work. Occasionally, it
also contains issues identified as won’t dos, duplicates, etc.

Additionally, our group-level board contains a column named “Tracking”
which is normally minimized. This column is used to track issues that
folks external to the DLD team are working on (e.g., content-related
accessibility improvements). These serve as a reminder that work on
these issues is in progress without cluttering the team board.

## Branching

We follow a [trunk-based branching model][trunk-based]. This means we use a
single mainline as the point for collaboration. Development takes place on
[short-lived branches][short-lived] which are merged back to the `trunk`
frequently. We try to avoid merge commits—this means branches must be rebased
on top of `trunk` before they are merged. Once work has been merged, the source
branch is deleted.

Many materials recommend specific frequencies (e.g. daily). In practice, we
submit [Merge Requests](#code-review) when substantial and clear progress has
been made. You don't need to finish a feature or ticket to merge; instead look
to submit work that is useful, non-breaking, and reasonably "done". If a branch
has lasted more than a few days, it might be a good time to ask: (1) is this
work scoped correctly and on track?; (2) are there portions of work that can be
quickly finished and submitted for review?

This branching model lets us:

  - prevent complicated conflicts from competing branches;
  - avoid lost/duplicated work due to stale branches;
  - forego coordination about where developers should branch from (always branch
    from `trunk`!);
  - keep our applications release-ready.

### Branch Etiquette

It’s a good idea to open a Merge Request for any work that you are actively
undertaking, even if it isn’t “ready”, so that others can comment and discuss
the potential changes. If your branch is a work‐in‐progress, you can label it a
draft by sticking `Draft: ` at the beginning of the title. In draft MRs, feel
free to work messily on whatever you are trying to implement.

Developers coming from GitHub may be reticent to “clean up” their commit history
in a branch with an open MR, but here it is generally encouraged. Having a
clean, logical set of commits helps to facilitate [review](#code-review) and
makes the eventual commit log on `trunk` much easier to read. Git tools like
`git commit --amend` and `git rebase -i` can be very useful in wrangling your
branch history into a presentable state.

If you are going to be force‐pushing anyway, it is a good idea to fetch and
rebase on top of `trunk` while you are at it, as all branches need to be rebased
on `trunk` prior to being merged.

One caveat applies to the above: If you are collaborating in a branch with
somebody, clear communication is important regarding any “dangerous” changes
that might require a force‐push. Usually, sending a message in Slack suffices as
a heads‐up about any branch rewriting which might be taking place.

## Commits

Individual commits should be _complete_ in the sense that they can individually
pass CI/CD, be merged to `trunk`, and deployed. They do not need to be
_complete_ in the sense of “completely implementing a feature”.

Consistent adherence to this practice means any commit on `trunk` can be
released and deployed at any time. This provides much needed flexibility and
confidence in situations where a revert or rollback is called for. Likewise,
a test suite that passes for each commit makes `git bisect` an invaluable tool
for tracking down regressions.

It can be useful, as a lone developer or a pair working on a short-lived branch,
to create commits that cannot pass CI. You might want to add a failing test as a
harness for further work, save exploratory work to pick up later, finish a
change to an API before updating callers, or change a system depenedency in the
development environment before updating infrastructure. All of these are good
patterns in development and can be tidied up (e.g. with `git rebase -i`) when
preparing work for merge to `trunk`.

### Commit Messages

Following [typical Git conventions][git-commit], commits should encapsulate a
single conceptual “change” and be described with an informative commit message.
Commit messages should consist of a summary (at most 50 characters) and an
extended description (hardwrapped to no more than 72 columns), separated by a
single blank line. Summaries should be conjugated in the infinitive and not end
in a period. The formatting of extended descriptions is left to the discretion
of the commit author.

Please do not skip the extended description unless your work really is trivial
or obvious. If you had to explore multiple routes when working on a commit,
document each and explain why you chose the route you did.

When a project consists of multiple components (i.e. the
[Project Surfliner][surfliner] monorepo), current practice is to begin the
commit message with the name of the component, followed by a colon (for example:
`comet: characterize uploads in the background`).

## Code Review

We submit code for integration using a Merge Request (MR) model. MRs are submitted and tracked in the GitLab
interface, and must be reviewed and approved by another teammate prior to a merge into `trunk`.

### Why Have Code Reviews?
Having another pair of eyes on each change that happens to the code help us improve the quality of the code
in the trunk and ensure we consistently deploy stable software. Code reviews can happen in person
or over the interwebs, and can happen as the code is being written (as part of pair programming).
Remember, the best reviewers understand not only the code but the requirements behind the code.

### What Code Review Is NOT:
Catching bugs is not a primary purpose of code reviews, though they often do. (Tests are a better way of catching bugs.)
Nor are code reviews meant to be an opportunity to rethink every decision the code author has ever made. Rather,
they are an essential part of code hygiene, making sure that a change is understandable, maintainable,
and a good fit with the direction of the project.

### Things to Consider When Reviewing

First, the person contributing the code is putting themselves out there. Be mindful of what you say in a review.

* Ask clarifying questions
* State your understanding and expectations
* Provide example code or alternate solutions, and explain why

This is your chance for a mentoring moment of another developer. Take time to give an honest and
thorough review of what has changed. Things to consider:

  * Does the commit message explain what is going on?
  * Does the code changes have appropriate tests?
  * Do new or changed methods, modules, and classes have documentation?
  * Does the commit contain more than it should? Are two separate concerns being addressed in one commit?
  * Does the description of the new/changed specs match your understanding of what the spec is doing?
  * Did the CI pipeline complete successfully?

If you are uncertain about the proposed changes in the Merge Request, please
provide this very valuable feedback by asking questions. It benefits the entire
team if Merge Requests are used as an opportunity to share knowledge. It is a
goal of the project to try and minimize silos of expertise. Please feel
comfortable using a Merge Request review as an opportunity to help us all meet
hat goal!

If, however, you find after this dialog that you are uncertain in approving the
Merge Request, feel free to bring other contributors into the conversation by
assigning them as a reviewer or mentioning the issue in Slack or daily stand up.

## Automated Dependency MRs

Occasionally merge requests will be opened by a user called `@dld-bot`.  This
account uses the [Renovate](https://github.com/renovatebot/renovate/) tool to
periodically scan repositories for out-of-date dependencies and open MRs to
update them.  It runs as a `CronJob` in our Kubernetes cluster, defined (as of
writing) as:

```yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: renovatebot
  namespace: bots
spec:
  schedule: '@hourly'
  concurrencyPolicy: Forbid
  jobTemplate:
    spec:
      template:
        spec:
          containers:
            - name: renovate
              image: renovate/renovate:25.43.0
              args:
                - ucsb-library/cylinders
                - ucsb-library/ezid.js
                - ucsb-library/library-website
                - ucsb-library/rds-dataverse
              envFrom:
                - secretRef:
                    name: renovatebot-env
          restartPolicy: Never
```

The `args` list defines which repositories are scanned; repos can be added or
removed as necessary.

`renovatebot-env` refers to a K8s `Secret` that defines the credentials
necessary for the program to run:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: renovatebot-env
  namespace: bots
type: Opaque
stringData:
  GITHUB_COM_TOKEN: '<secret>' # optional
  RENOVATE_AUTODISCOVER: 'false'
  RENOVATE_ENDPOINT: 'https://gitlab.com/api/v4'
  RENOVATE_GIT_AUTHOR: 'Renovate Bot <bot@renovateapp.com>'
  RENOVATE_PLATFORM: 'gitlab'
  RENOVATE_TOKEN: '<secret>'
```

For more details see the [upstream documentation](https://github.com/renovatebot/renovate/blob/main/docs/usage/examples/self-hosting.md#kubernetes).

## Continuous Integration and Deployment

Continuous integration and deployment is the process of, well, _continuously_
testing and deploying software, in order to quickly catch bugs or configuration
errors.  In our case this means:

- for merge requests, automatically running tests when the MR is opened, and
  creating a temporary review instance incorporating the proposed changes, to
  exercise the full deployment pipeline and to allow for manual QA.

- for commits to the `trunk`/`main` branch of the repository, running the tests
  again, then (if they pass), automatically deploying the new code to our
  staging and production instances.  This ensures that we are always running the
  latest version of our code.

Projects hosted on GitLab use the native [GitLab
CI](https://docs.gitlab.com/ee/ci/) for testing and deploying applications.  A
very simple example of a GitLab CI configuration is the one given for the
[ezid.js
module](https://gitlab.com/ucsb-library/ezid.js/-/blob/trunk/.gitlab-ci.yml); a
runner executes the specified test command on the specified Docker image.

The [cylinders
project](https://gitlab.com/ucsb-library/cylinders/-/blob/trunk/.gitlab-ci.yml)
offers a much more complex CI configuration.  Rather than running commands on
pre-existing images, we build our own using
[Kaniko](https://github.com/GoogleContainerTools/kaniko), then deploy them into
the DLD Kubernetes environment.  The pipeline runs as follows:

1. in the
   [build](https://gitlab.com/ucsb-library/cylinders/-/blob/trunk/ci/cylinders/build.yml)
   stage, we create the application image and store it in the GitLab [container
   registry](https://gitlab.com/ucsb-library/cylinders/container_registry)
1. if this is a merge request, we create a [review
   deployment](https://gitlab.com/ucsb-library/cylinders/-/blob/trunk/ci/cylinders/review.yml)
   by installing an ephemeral instance (without persistent storage and other
   production-grade configurations) of the cylinders application into our K8s
   cluster.  When the MR is merged or closed, the instance is uninstalled.
1. when a commit is pushed to `trunk`, we deploy production-grade instances with
   the
   [staging](https://gitlab.com/ucsb-library/cylinders/-/blob/trunk/ci/cylinders/staging.yml)
   and
   [production](https://gitlab.com/ucsb-library/cylinders/-/blob/trunk/ci/cylinders/production.yml)
   builds

A pipeline may include other steps, such as running automated tests or checking
the code quality.

The engineering team as a whole is responsible for the maintanence of the CI/CD
pipelines, with primary responsibility for each project falling to that project
owner.

[git-commit]: https://cbea.ms/git-commit/
[git]: https://www.git-scm.com/
[short-lived]: https://trunkbaseddevelopment.com/short-lived-feature-branches/
[surfliner]: https://gitlab.com/surfliner/surfliner
[trunk-based]: https://trunkbaseddevelopment.com/
[ucsb-gitlab]: https://gitlab.com/ucsb-library
